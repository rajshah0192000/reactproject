import React, { Component } from 'react';
// import axios from 'axios';
// import Sum from './Sum.jsx';

class Sum extends Component {
  constructor(props) {
    super(props)
    this.state = {
      posts: []
    }
  }
  componentDidMount() {
    fetch('data/data.json'
    ,{
      headers : { 
        'Content-Type': 'application/json',
        'Accept': 'application/json'
       }
    }
    )
      .then(function(response){
        // console.log(response)
        return response.json();
      })
      .then((myJson)=> {
        // console.log(myJson);
        this.setState({ posts:myJson.data.purchased_services });
      });
  }
  render() {
    const { posts } = this.state
    console.log("post data",posts)
    return (
      <>
        <div className="card text-white bg-dark">
            {
              Object.keys(posts).map((post, index) => (
                ((posts[post].purchased_office_template.purchased_office_services[0].service_selected) ?

                <div className="row">
                  <div className="col-sm-9">
                    <h3 className="card-text">{posts[post].purchased_office_template.purchased_office_services[0].name}</h3>
                  </div>
                  <div className="col-sm-3">
                    <h3 className="card-text"><b>{posts[post].purchased_office_template.purchased_office_services[0].price}</b></h3>
                  </div>
                </div>: null)

              ))
            }<hr />
          <div className="row">
            <div className="col-sm-9"><h2><b>Total Is :</b></h2></div>
         <div className="col-sm-3"><h2><b>6000.00</b></h2></div>
          
          </div>
        
      </div>
      </>
    )
  }
}
export default Sum;